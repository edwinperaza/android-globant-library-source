package com.globant.android_globant_library.bases;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.globant.android_globant_library.R;
import com.globant.cencosudsupport.floatingsearchbar.FloatingSearchView;

/**
 * Created by raulstriglio on 6/7/17.
 */

public class BaseJumboActivity extends AppCompatActivity {

    private ViewGroup mBaseActivityLayout;
    private ViewGroup mMainContainer;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        mBaseActivityLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.base_activity, null);
        mMainContainer = (ViewGroup) mBaseActivityLayout.findViewById(R.id.main_container);
        getLayoutInflater().inflate(layoutResID, mMainContainer, true);

        setContentView(mBaseActivityLayout);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void goToFragment(int layout, Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(layout, fragment, tag).commit();
    }

    public ViewGroup getMainContainer() {
        return mMainContainer;
    }

    public void showConnectionSnackbar(View view) {
        BaseJumboActivityView.showConnectionSnackbar(view);
    }

    public void configureToolbarBackArrow(boolean show) {

        ActionBar actionBar = getSupportActionBar();
        if (show) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }
        actionBar.setDisplayHomeAsUpEnabled(show);
    }

    public void configureOpenSearchView(FloatingSearchView mFloatingSearchView, boolean focused) {
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back);
        mFloatingSearchView.setDrawableToLeftIcon(DrawableCompat.wrap(drawable));
        mFloatingSearchView.showLeftIcon(focused);
        mFloatingSearchView.showRightIcon(!focused);
        if (focused) {
            configureToolbarBackArrow(false);
        }
        mFloatingSearchView.setVisibility(View.VISIBLE);

    }

    public void configureSearchFirstLevelSearchBarWithBackIcon(FloatingSearchView mFloatingSearchView) {
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.search);
        mFloatingSearchView.setDrawableToLeftIcon(DrawableCompat.wrap(drawable));
        mFloatingSearchView.showLogo(false);
        mFloatingSearchView.showLeftIcon(false);
        mFloatingSearchView.showRightIcon(true);
        mFloatingSearchView.setVisibility(View.VISIBLE);
        configureToolbarBackArrow(true);
    }

    public void configureSearchFirstLevelSearchBarWithoutBackIcon(FloatingSearchView mFloatingSearchView) {

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.search);
        mFloatingSearchView.setDrawableToRightIcon(DrawableCompat.wrap(drawable));
        mFloatingSearchView.showLogo(false);
        mFloatingSearchView.showLeftIcon(false);
        mFloatingSearchView.showRightIcon(true);
        mFloatingSearchView.setVisibility(View.VISIBLE);
    }

    public void configureSearchBarDeepLevel(FloatingSearchView mFloatingSearchView) {
        Drawable drawable = null;
        mFloatingSearchView.showLeftIcon(true);
        mFloatingSearchView.showRightIcon(false);
        drawable = ContextCompat.getDrawable(this, R.drawable.search);
        drawable = DrawableCompat.wrap(drawable);
        mFloatingSearchView.setDrawableToLeftIcon(drawable);
        mFloatingSearchView.showLogo(false);
        mFloatingSearchView.setVisibility(View.VISIBLE);
    }

    public void hideSearchBarComponents(FloatingSearchView mFloatingSearchView) {
        mFloatingSearchView.showLeftIcon(false);
        mFloatingSearchView.showRightIcon(false);
        mFloatingSearchView.showLogo(false);
        mFloatingSearchView.setVisibility(View.GONE);
    }
}
