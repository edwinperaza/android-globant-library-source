package com.globant.android_globant_library.bases;

/**
 * Created by raulstriglio on 6/7/17.
 * Updated by edwinperaza on 12/7/17.
 */

public class BasePresenter<T> {

    protected BaseModel baseModel;
    protected T baseView;

    public BasePresenter(BaseModel model, T view) {
        this.baseModel = model;
        this.baseView = view;
    }

    /**
     * When user add a product to cart we need to show a message and update specific
     * item view, so we need to implement this method on child class for example: CatalogPresenter
     * to be able to do this behavior
     *
     * @param position to be updated at the list
     */
    public void cartUpdateCallback(int position) { }
}
