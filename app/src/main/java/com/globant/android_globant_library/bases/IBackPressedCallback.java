package com.globant.android_globant_library.bases;

/**
 * Created by edwin.peraza on 6/15/2017
 *
 * Interface to manage back press event on Fragments
 */
public interface IBackPressedCallback {
    void onFragmentBackPressed();
}