package com.globant.android_globant_library.bases;

import com.squareup.otto.Bus;

/**
 * Created by raulstriglio on 6/7/17.
 */

public class BaseModel {
    protected Bus mBus;

    public BaseModel(Bus bus) {
        mBus = bus;
    }

}
