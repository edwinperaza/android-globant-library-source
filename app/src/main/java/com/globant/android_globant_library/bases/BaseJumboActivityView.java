package com.globant.android_globant_library.bases;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.globant.android_globant_library.R;
import com.globant.android_globant_library.utils.BusProvider;
import com.squareup.otto.Bus;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by raulstriglio on 6/7/17.
 */

public class BaseJumboActivityView {

    protected WeakReference<BaseJumboActivity> activityRef;
    protected Bus mBus;

    @BindView(R.id.dialog_layout)
    View modal;
    @BindView(R.id.pb_base)
    View loadingOverlay;

    private boolean mIsModalVisible;
    private boolean mKeyboardIsVisible;

    public BaseJumboActivityView(BaseJumboActivity activity) {
        activityRef = new WeakReference<>(activity);
        ButterKnife.bind(this, activity);
        mBus = BusProvider.getInstance();
    }

    public BaseJumboActivityView(BaseJumboActivity activity, Bus bus) {
        activityRef = new WeakReference<>(activity);
        this.mBus = bus;
        ButterKnife.bind(this, activity);
    }

    @Nullable
    public AppCompatActivity getActivity() {
        return activityRef.get();
    }

    @Nullable
    public Context getContext() {
        return getActivity();
    }

    private void closeModal() {
        modal.setVisibility(View.GONE);
        mIsModalVisible = false;
    }

    public void showModal(String title,
                          String description,
                          String positiveButtonText,
                          final View.OnClickListener listenerPositive,
                          boolean showNegativeButton) {
        hideLoadingOverlay();
        mIsModalVisible = true;
        getActivity().findViewById(R.id.editText_edit_mode).setVisibility(View.GONE);
        if (title.isEmpty()) {
            getActivity().findViewById(R.id.dialog_title).setVisibility(View.GONE);
        } else {
            getActivity().findViewById(R.id.dialog_title).setVisibility(View.VISIBLE);
            ((TextView) getActivity().findViewById(R.id.dialog_title)).setText(title);
        }
        if (description.isEmpty()) {
            getActivity().findViewById(R.id.dialog_description).setVisibility(View.GONE);
        } else {
            getActivity().findViewById(R.id.dialog_description).setVisibility(View.VISIBLE);
            ((TextView) getActivity().findViewById(R.id.dialog_description)).setText(description);
        }

        final View.OnClickListener listenerCloseModal = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeModal();
                showKeyboard(getActivity().findViewById(R.id.editText_edit_mode));
                hideKeyboard();
            }
        };

        final TextView positiveButton = (TextView) getActivity().findViewById(R.id.btn_accept);
        positiveButton.setText(positiveButtonText);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listenerPositive != null) {
                    listenerPositive.onClick(view);
                }
                listenerCloseModal.onClick(view);
            }
        });
        positiveButton.setVisibility(View.VISIBLE);

        TextView negativeButton = (TextView) getActivity().findViewById(R.id.btn_cancel);
        if (showNegativeButton) {
            negativeButton.setVisibility(View.VISIBLE);
            negativeButton.setOnClickListener(listenerCloseModal);
        } else {
            negativeButton.setVisibility(View.GONE);
        }
        modal.setVisibility(View.VISIBLE);
    }

    public void showLoadingOverlay() {
        loadingOverlay.setVisibility(View.VISIBLE);
    }

    public void hideLoadingOverlay() {
        loadingOverlay.setVisibility(View.GONE);
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mKeyboardIsVisible = false;
    }

    protected void showKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, 0);
        }
        mKeyboardIsVisible = true;
    }

    protected boolean isKeyboardOpen() {
        return mKeyboardIsVisible;
    }

    protected boolean isLoadingOverlayShowing() {
        return loadingOverlay.getVisibility() == View.VISIBLE;
    }

    /**
     * Gets the state of Airplane Mode.
     *
     * @param context
     * @return true if enabled.
     */
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        }

    }

    public static boolean isConnectionOff(Context context) {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        return info == null || !info.isConnectedOrConnecting();
    }

    public static void showConnectionSnackbar(View view) {
        TSnackbar snackbar = TSnackbar.make(view, "Sin Conexion", TSnackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setAlpha(0.7f);
        snackbarView.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
