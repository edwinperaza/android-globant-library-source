package com.globant.android_globant_library.main.activities;

import android.os.Bundle;

import com.globant.android_globant_library.R;
import com.globant.android_globant_library.bases.BaseJumboActivity;
import com.globant.android_globant_library.main.model.MainModel;
import com.globant.android_globant_library.main.presenter.MainPresenter;
import com.globant.android_globant_library.main.view.MainView;
import com.globant.android_globant_library.utils.BusProvider;

public class MainActivity extends BaseJumboActivity {

    private static final String MAIN_ACTIVITY_TAG = MainActivity.class.getSimpleName();
    private MainView mMainView;
    private MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainView = new MainView(this);
        mMainPresenter = new MainPresenter(this, new MainModel(BusProvider.getInstance()), mMainView);
        BusProvider.register(mMainPresenter);
        mMainPresenter.setMainView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(mMainPresenter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
