package com.globant.android_globant_library.main.model;

import com.globant.android_globant_library.bases.BaseModel;
import com.squareup.otto.Bus;

import java.util.HashMap;

/**
 * Created by Jotta Reyes Blanco on 6/16/2017.
 */

public class MainModel extends BaseModel {

    public MainModel(Bus bus) {
        super(bus);
    }
}