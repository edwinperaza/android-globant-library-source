package com.globant.android_globant_library.main.presenter;

import android.util.Log;

import com.globant.android_globant_library.bases.BasePresenter;
import com.globant.android_globant_library.main.activities.MainActivity;
import com.globant.android_globant_library.main.model.MainModel;
import com.globant.android_globant_library.main.view.MainView;
import com.squareup.otto.Bus;

import java.util.HashMap;

/**
 * Created by Jotta Reyes Blanco on 6/16/2017
 * Changed by Mariano Salvetti
 */

public class MainPresenter extends BasePresenter<MainView> {

    private static final String MAIN_PRESENTER_TAG = MainPresenter.class.getSimpleName();

    private MainView mView;
    private MainModel mModel;
    private MainActivity mMainActivity;
    private Bus mBus;
    private boolean mIsFromPdp;

    public MainPresenter(MainModel model, MainView view) {
        super(model, view);
        mModel = model;
        mView = view;
    }

    public MainPresenter(MainActivity mainActivity, MainModel model, MainView view) {
        super(model, view);
        mModel = model;
        mView = view;
        mMainActivity = mainActivity;
    }

    public void setMainView() {
        onMainViewResponse();
    }

    private void onMainViewResponse() {
        mView.populateView();
        Log.d(MAIN_PRESENTER_TAG, "onMainViewResponse");
    }

}
