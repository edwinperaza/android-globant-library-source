package com.globant.android_globant_library.main.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.globant.android_globant_library.R;
import com.globant.android_globant_library.bases.BaseJumboActivity;
import com.globant.android_globant_library.bases.BaseJumboActivityView;
import com.globant.cencosudsupport.floatingsearchbar.FloatingSearchView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Edwin Peraza on 6/26/2017
 * Mariano Salvetti
 **/

public class MainView extends BaseJumboActivityView {

    private static String MAIN_VIEW_TAG = MainView.class.getSimpleName();
    private String mQuery;
    private Context mContext;
    private Resources mResources;

    //BindView Toolbar
    @BindView(R.id.pd_iv_back_button)
    ImageView ivDetailsBackButton;

    @BindView(R.id.pd_iv_shopping_cart)
    ImageView mShoppingCartImageView;

    @BindView(R.id.pd_menu_cart_text)
    TextView mShoppingCartTextView;

    @BindView(R.id.activity_product_detail)
    ConstraintLayout activityProductDetail;

    @BindView(R.id.cv_searchbar)
    CardView mCvSearchBar;

    @BindView(R.id.floating_search_view)
    FloatingSearchView mFloatingSearchView;

    public MainView(BaseJumboActivity activity) {
        super(activity);
    }

    public void populateView() {
        mContext = getContext();
        bindSearchBarViewElements();
        mResources = mContext.getResources();
    }

    public View getMainViewContainer() {
        return activityProductDetail;
    }

    @OnClick(R.id.pd_iv_back_button)
    public void onBackButtonPressed() {
        getActivity().onBackPressed();
    }

    //Configure searchBar section
    /**
     * Bind element from view to Activity
     */
    public void bindSearchBarViewElements() {
        mFloatingSearchView.setActivated(false);
        //configureSearchFirstLevelSearchBarWithoutBackIcon(mFloatingSearchView);
        configureOpenSearchViewAtHome(mFloatingSearchView, true);
        setupSearchView();
    }

    public void configureSearchFirstLevelSearchBarWithoutBackIcon(FloatingSearchView mFloatingSearchView) {
        Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.search_pdp);
        mFloatingSearchView.setDrawableToRightIcon(DrawableCompat.wrap(drawable));
        mFloatingSearchView.showLogo(false);
        mFloatingSearchView.showLeftIcon(false);
        mFloatingSearchView.showRightIcon(true);
        showClearButton(false);
    }

    public void configureOpenSearchViewAtHome(FloatingSearchView mFloatingSearchView, boolean focused) {
        Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.search_pdp);
        mFloatingSearchView.setDrawableToLeftIcon(DrawableCompat.wrap(drawable));
        mFloatingSearchView.showLeftIcon(!focused);
        mFloatingSearchView.showRightIcon(!focused);
/*        if (focused) {
            configureToolbarBackArrow(false);
        }*/
        mFloatingSearchView.setVisibility(View.VISIBLE);
        mFloatingSearchView.setVisibilityAndAvailabilityToSearchInputEditText(true);
    }


    protected void showClearButton(boolean show) {
        mFloatingSearchView.getMenu().findItem(R.id.menu_clear).setVisible(show);
        mFloatingSearchView.getMenu().findItem(R.id.menu_clear).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_clear:
                        mQuery = "";
                        mFloatingSearchView.setText(null);
                        mFloatingSearchView.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                        break;
                }
                return true;
            }
        });
    }

    protected void configureSearchBar() {
        mFloatingSearchView.setActivated(!mFloatingSearchView.isActivated());
    }

    public void configureOpenSearchView(FloatingSearchView mFloatingSearchView, boolean focused) {
        Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.arrow_back);
        mFloatingSearchView.setDrawableToLeftIcon(DrawableCompat.wrap(drawable));
        mFloatingSearchView.showLeftIcon(focused);
        mFloatingSearchView.showRightIcon(!focused);
        if (focused) {
            ivDetailsBackButton.setVisibility(View.GONE);
            mShoppingCartImageView.setVisibility(View.GONE);
            mShoppingCartTextView.setVisibility(View.GONE);
            mCvSearchBar.setCardElevation(5);
            ViewGroup.MarginLayoutParams layoutParams =
                    (ViewGroup.MarginLayoutParams) mCvSearchBar.getLayoutParams();
            layoutParams.setMargins(15, 12, 60, 0);
            mCvSearchBar.requestLayout();

        } else {

            mQuery = "";
            ivDetailsBackButton.setVisibility(View.VISIBLE);
            mShoppingCartImageView.setVisibility(View.VISIBLE);
            mShoppingCartTextView.setVisibility(View.VISIBLE);
            mCvSearchBar.setCardElevation(0);

            ViewGroup.MarginLayoutParams layoutParams =
                    (ViewGroup.MarginLayoutParams) mCvSearchBar.getLayoutParams();
            layoutParams.setMargins(0, 12, 0, 0);
            mCvSearchBar.requestLayout();
        }
    }

    private void setupSearchView() {

        final FloatingSearchView.LogoEditText mSearchEditText = (FloatingSearchView.LogoEditText) mFloatingSearchView.findViewById(R.id.search_text);
        showClearButton(false);
        mFloatingSearchView.getmFloatingSearchViewInputEditText().setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        mFloatingSearchView.setOnIconClickListener(new FloatingSearchView.OnIconClickListener() {
            @Override
            public void onNavigationClick() {
                mFloatingSearchView.setText(null);
                configureSearchBar();
            }
        });

        mFloatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSearchAction(CharSequence text) {
                mFloatingSearchView.setActivated(false);

            }
        });

        mFloatingSearchView.setOnSearchFocusChangedListener(new FloatingSearchView.OnSearchFocusChangedListener() {
            @Override
            public void onFocusChanged(final boolean focused) {
                boolean textEmpty = mFloatingSearchView.getText().length() == 0;
                showClearButton(focused && !textEmpty);
                mFloatingSearchView.showLogo(focused && !textEmpty);
                if (focused && mQuery != null && !mQuery.isEmpty()) {
                    mFloatingSearchView.setText(mQuery);
                }
                //configureOpenSearchView(mFloatingSearchView, focused);
                configureOpenSearchView(mFloatingSearchView, true);
            }
        });


        mFloatingSearchView.addTextChangedListener(new TextWatcher() {
            boolean isTyping = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                showClearButton(query.length() > 0 && mFloatingSearchView.isActivated());
                if (query.length() > 0 && mFloatingSearchView.isActivated()) {
                    mFloatingSearchView.setSearchActiveBaseSeparatorVisibility(View.VISIBLE);
                } else {
                    mFloatingSearchView.setSearchActiveBaseSeparatorVisibility(View.INVISIBLE);
                }
            }

            private Timer mTimer = new Timer();
            private final long mDelay = 10000; // milliseconds

            @Override
            public void afterTextChanged(final Editable s) {
                if (!isTyping) {
                    // Send notification for start typing event
                    isTyping = true;
                }

                if (s.length() >= 2) {

                    mTimer.cancel();
                    mTimer = new Timer();
                    mTimer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    isTyping = false;
                                    mQuery = s.toString();
                                }
                            },
                            mDelay
                    );
                }
            }
        });

        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    mQuery = mFloatingSearchView.getText().toString();
                    performSearch(mQuery);
                    mFloatingSearchView.setVisibilityAndAvailabilityToSearchInputEditText(false);
                }
                return false;
            }
        });
        mFloatingSearchView.setSearchActiveBaseSeparatorColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        mFloatingSearchView.setSearchInputEditTextBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGreen));
        mFloatingSearchView.setSearchInputEditTextBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.background_edit_text));
        mFloatingSearchView.setActionMenuBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGreen));
        mFloatingSearchView.setBackgroundDrawableToInputLeftIcon(ContextCompat.getDrawable(mContext, R.drawable.background_edit_text));
    }

    private void performSearch(String mQuery) {
        Toast.makeText(mContext, "Busqueda Realizada", Toast.LENGTH_LONG).show();
    }

}