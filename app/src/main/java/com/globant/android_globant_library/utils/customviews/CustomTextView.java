package com.globant.android_globant_library.utils.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.globant.cencosudsupport.font.TypeFaceManager;


public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
           /* TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            int typeface = values.getInt(R.styleable.CustomTextView_typeface, 0);
            setTypeface(TypeFaceManager.getFont(typeface, getContext()));
            values.recycle();*/
        }
    }

}