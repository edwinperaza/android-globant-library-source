package com.globant.cencosudsupport.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Mariano Salvetti.
 * <p>
 * Products Repository
 * 1) In our App, the products have a role of "slave", ie in the app only be consulted.
 * 2) The user will not be able to create, modify or eliminate products.
 * Then, if there is data in memory and no recharging was ordered, they will be displayed directly
 * without run a request to the server. If it is a reload, by means of the variable mReload, then we
 * ask for data to the server.
 **/

public class BaseRepository {

    private final ConnectivityManager mConnectivityManager;

    protected final String NO_NETWORK_ERROR = "No hay conexión de red.";
    protected final String GENERIC_RESPONSE_ERROR = "Ocurrió un error";


    public BaseRepository(Context context) {
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    protected boolean isOnline() {
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }
}
