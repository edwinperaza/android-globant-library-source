package com.globant.cencosudsupport.bannerslider.events;

/**
 * Created by Jotta Reyes Blanco on 2/8/2017.
 */

public interface OnBannerClickListener {
    void onClick(int position);
}
