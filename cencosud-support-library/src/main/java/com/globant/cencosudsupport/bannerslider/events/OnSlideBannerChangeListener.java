package com.globant.cencosudsupport.bannerslider.events;

/**
 * Created by Jotta Reyes Blanco on 2/8/2017
 * rename.
 */

public interface OnSlideBannerChangeListener {

    void onSlideChange(int selectedSlidePosition);
}
