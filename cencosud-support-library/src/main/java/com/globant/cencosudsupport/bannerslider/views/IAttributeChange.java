package com.globant.cencosudsupport.bannerslider.views;

/**
 * Created by Jotta Reyes Blanco on 2/8/2017.
 */

public interface IAttributeChange {
    void onIndicatorSizeChange();
    void onSelectedSlideIndicatorChange();
    void onUnselectedSlideIndicatorChange();
    void onDefaultIndicatorsChange();
    void onAnimateIndicatorsChange();
    void onIntervalChange();
    void onLoopSlidesChange();
    void onDefaultBannerChange();
    void onEmptyViewChange();
    void onHideIndicatorsValueChanged();
}
