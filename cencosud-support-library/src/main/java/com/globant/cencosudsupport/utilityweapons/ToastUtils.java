package com.globant.cencosudsupport.utilityweapons;

import android.content.Context;
import android.widget.Toast;

import com.globant.cencosudsupport.swiperefresh.R;

/**
 * Created by mariano.salvetti on 03/02/2016
 *  Toast utility for the App.
 *
 */
public class ToastUtils {

    public static void show(CharSequence text, int duration, Context context) {
        Toast.makeText(context, text, duration).show();
    }
    public static void show(int resId, int duration, Context context) {
        show(context.getText(resId), duration, context);
    }

    public static void show(CharSequence text, Context context) {
        show(text, Toast.LENGTH_SHORT, context);
    }

    public static void show(int resId, Context context) {
        show(context.getText(resId), context);
    }

    public static void showNotYetImplemented(Context context) {
        show(R.string.not_yet_implemented, context);
    }
}
