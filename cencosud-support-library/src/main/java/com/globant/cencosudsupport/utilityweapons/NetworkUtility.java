package com.globant.cencosudsupport.utilityweapons;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by mariano.salvetti on 21/12/2015
 * Network utility for the App.
 */
public class NetworkUtility {

    private static final String COOKIE_VALUE_DELIMITER = ";";
    private static final String COOKIE_KEY_VTEX = "checkout.vtex.com";
    private static final String COOKIE_KEY_OFID = "__ofid";
    private static final String NAME_VALUE_SEPARATOR = "=";


    public boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public String parseCookies(String cookieParam) {
        String ofId = "";
        // OK, now we are ready to get the cookies out of the URLConnection
        Map cookie = new HashMap();
        StringTokenizer st = new StringTokenizer(cookieParam, COOKIE_VALUE_DELIMITER);

        // the specification dictates that the first name/value pair
        // in the string is the cookie name and value, so let's handle
        // them as a special case:
        while (st.hasMoreTokens()) {
            String token  = st.nextToken();
            String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
            String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());

            if (name.trim().equals(COOKIE_KEY_VTEX)) {
                String idName = value.substring(0, value.indexOf(COOKIE_KEY_OFID));
                ofId = value.substring(value.indexOf(NAME_VALUE_SEPARATOR) + 1, value.length());
            }
            cookie.put(name, value);
        }
        return ofId;
    }
}