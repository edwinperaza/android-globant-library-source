package com.globant.cencosudsupport.svg;

import android.annotation.TargetApi;
import android.graphics.drawable.PictureDrawable;
import android.os.Build;
import android.os.Handler;
import android.widget.ImageView;

import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.target.Target;

/**
 * Created by mariano.salvetti on 11/5/2017.
 * Listener which updates the {@link ImageView} to be software rendered,
 * because com.caverock.androidsvg.SVG android.graphics.Picture
 * can't render on a hardware backed {@link android.graphics.Canvas Canvas}.
 *
 * @param <T> not used, here to prevent unchecked warnings at usage
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SvgSoftwareLayerSetter<T> implements RequestListener<T, PictureDrawable> {

    private IFinishLoadingCallback mIFinishLoadingCallback;
    private int position;
    private final int MIN_TIME_TO_REFRESH_IMAGE = 20;

    public SvgSoftwareLayerSetter() {

    }

    public SvgSoftwareLayerSetter(IFinishLoadingCallback iFinishLoadingCallback, int position) {
        mIFinishLoadingCallback = iFinishLoadingCallback;
        this.position = position;
    }

    @Override
    public boolean onException(Exception e, T model, Target<PictureDrawable> target, boolean isFirstResource) {
        ImageView view = ((ImageViewTarget<?>) target).getView();
        if (Build.VERSION_CODES.HONEYCOMB <= Build.VERSION.SDK_INT) {
            view.setLayerType(ImageView.LAYER_TYPE_NONE, null);
        }

        if (mIFinishLoadingCallback != null) {
            mIFinishLoadingCallback.onExceptionCallback();
        }

        return false;
    }

    @Override
    public boolean onResourceReady(PictureDrawable resource, T model, Target<PictureDrawable> target,
                                   boolean isFromMemoryCache, boolean isFirstResource) {
        ImageView view = ((ImageViewTarget<?>) target).getView();
        if (Build.VERSION_CODES.HONEYCOMB <= Build.VERSION.SDK_INT) {
            view.setLayerType(ImageView.LAYER_TYPE_SOFTWARE, null);
        }

        if (mIFinishLoadingCallback != null) {

            if (mIFinishLoadingCallback.isFromDepartments()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        mIFinishLoadingCallback.onResourceReadyCallback(position);
                    }
                }, MIN_TIME_TO_REFRESH_IMAGE);
            } else {
                mIFinishLoadingCallback.onResourceReadyCallback(position);
            }

        }

        return false;
    }

    public interface IFinishLoadingCallback {

        void onExceptionCallback();

        void onResourceReadyCallback(int position);

        boolean isFromDepartments();

    }
}