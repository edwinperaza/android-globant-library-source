package com.globant.cencosudsupport.font;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * This class manages the custom TypeFace objects used on the Application.
 * It stores the TypeFaces on a private static HashMap caching them and
 * retrieve them when requested.
 * <p>
 * Storing the TypeFaces on the HashMap allows to call Typeface.createFromAsset method just
 * only one time per font, avoiding memory leak issues.
 *
 * @author Mariano Salvetti
 */
public class TypeFaceManager {

    /**
     * Enum for the Custom Fonts that are used on the Application.
     * The value is the font filename located in the assets/fonts folder of the project.
     * <p>
     * The order of these fonts has to be the same as the one on attrs.xml file,
     * on typeface attribute.
     */
    public enum CustomFonts {
        ROBOTO_REGULAR("roboto-regular.ttf"),
        ROBOTO_LIGHT("roboto-light.ttf"),
        ROBOTO_BOLD("roboto-bold.ttf"),
        CERA_BOLD("cera-pro-bold.otf"),
        CERA_BLACK("cera-pro-black.otf"),
        CERA_LIGHT("cera-pro-light.otf"),
        CERA_MEDIUM("cera-pro-medium.otf"),
        CERA_REGULAR("cera-pro-regular.otf"),
        CERA_THIN("cera-pro-thin.otf"),
        SOURCE_SANS_PRO_REGULAR("SourceSansPro-Regular.ttf"),
        SOURCE_SANS_PRO_SEMI_BOLD("SourceSansPro-SemiBold.ttf"),
        SOURCE_SANS_PRO_LIGHT("SourceSansPro-Light.ttf"),
        SOURCE_SANS_PRO_BOLD("SourceSansPro-Bold.ttf"),
        SOURCE_SANS_PRO_ITALIC("SourceSansPro-Italic.ttf"),
        NEXA_RUST_SANS_BLACK("NexaRustSans-Black.otf"),
        NEXA_RUST_SANS_BLACK01("NexaRustSans-Black01.otf"),
        SOURCE_SANS_PRO_BLACK("SourceSansPro-Black.ttf");

        private final String mFontFileName;

        CustomFonts(String mFontFileName) {
            this.mFontFileName = mFontFileName;
        }

        public String getmFontFileName() {
            return mFontFileName;
        }
    }

    private static final String FONTS_FOLDER = "fonts";
    private static HashMap<String, Typeface> cachedFonts = new HashMap<String, Typeface>();

    public static Typeface getFont(int typeface, Context context) {
        String fontName = CustomFonts.values()[typeface].getmFontFileName();
        Typeface font = cachedFonts.get(fontName);
        if (font == null) {
            font = Typeface.createFromAsset(context.getAssets(), FONTS_FOLDER + "/" + fontName);
            cachedFonts.put(fontName, font);
        }
        return font;
    }
}
