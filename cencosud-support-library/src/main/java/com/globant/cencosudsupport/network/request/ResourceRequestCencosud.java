package com.globant.cencosudsupport.network.request;

import com.worklight.wlclient.api.WLResourceRequest;
import com.worklight.wlclient.api.WLResponseListener;

import org.json.JSONObject;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edwin.peraza on 10/23/2017.
 *
 * Class to set values to WLResourceRequest and send request using IBM Mobile First
 */

public class ResourceRequestCencosud extends WLResourceRequest {

    WLResourceRequest mRequest;

    public ResourceRequestCencosud(ResourceRequestBuilder resourceRequestBuilder) {
        super(resourceRequestBuilder.mPath, resourceRequestBuilder.mMethod);

        mRequest = new WLResourceRequest(resourceRequestBuilder.mPath, resourceRequestBuilder.mMethod);
        mRequest.addHeader(resourceRequestBuilder.mHeaderName, resourceRequestBuilder.mHeaderValue);
        mRequest.setTimeout(resourceRequestBuilder.mTimeOut);
    }

    public void setQueryParamsToRequest(HashMap<String, String> parameters) {
        mRequest.setQueryParameters(parameters);
    }

    public void setQueryParamsToRequest(String name, String value) {
        mRequest.setQueryParameter(name, value);
    }

    public void sendRequest(WLResponseListener responseListener) {
        mRequest.send(responseListener);
    }

    public void sendRequest(String body, WLResponseListener responseListener) {
        mRequest.send(body, responseListener);
    }

    public void sendRequest(JSONObject json, WLResponseListener responseListener) {
        mRequest.send(json, responseListener);
    }

    public void sendRequest(Map<String, String> params, WLResponseListener responseListener) {
        mRequest.send(params, responseListener);
    }

    public void sendRequest(byte[] data, WLResponseListener responseListener) {
        mRequest.send(data, responseListener);
    }

    public static class ResourceRequestBuilder {
        private URI mPath;
        private String mMethod;
        private String mHeaderName;
        private String mHeaderValue;
        private int mTimeOut;

        public ResourceRequestBuilder(URI path, String  method) {
            this.mPath = path;
            this.mMethod = method;
        }

        public ResourceRequestBuilder addHeader(String name, String value) {
            this.mHeaderName = name;
            this.mHeaderValue = value;
            return this;
        }

        public ResourceRequestBuilder timeOut(int mTimeOut) {
            this.mTimeOut = mTimeOut;
            return this;
        }

        public ResourceRequestCencosud build() {
            return new ResourceRequestCencosud(this);
        }

    }
}