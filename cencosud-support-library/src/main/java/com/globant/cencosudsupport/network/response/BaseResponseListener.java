package com.globant.cencosudsupport.network.response;

import android.util.Log;

import com.globant.cencosudsupport.network.model.ErrorResponse;
import com.globant.cencosudsupport.network.request.IBaseCallback;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.WLResponseListener;

/**
 * Created by raul.striglio on 13/06/17.
 * changed by Mariano Salvetti and Edwin Peraza
 */

public abstract class BaseResponseListener implements WLResponseListener {

    protected String mErrorGeneric;
    protected Bus mBus;

    public BaseResponseListener(Bus bus) {
        mBus = bus;
    }

    @Override
    public void onSuccess(WLResponse wlResponse) {
        onSuccessResponse(wlResponse);
    }

    @Override
    public void onFailure(WLFailResponse wlFailResponse) {
        onSuccessError(wlFailResponse);
    }

    protected boolean responseIsEmpty(WLResponse response) {
        final int MIN_LENGTH_RESPONSE = 4;
        return (response.getResponseText().isEmpty() || response.getResponseText().length() < MIN_LENGTH_RESPONSE);
    }

    /**
     * Validate if the response is with Error Code or Empty
     *
     * @param response the response from server
     */
    protected void validateResponseError(WLResponse response, IBaseCallback callback) {

        if (responseErrorCode(response) || responseIsEmpty(response)) {
            try {
                ErrorResponse er = new Gson().fromJson(response.getStatusText(), ErrorResponse.class);
                mErrorGeneric = er.getMessage();
            } catch (Exception e) {
                Log.e("response error: ", e.getMessage());
            }
            callback.onError(mErrorGeneric);
        }
    }

    protected boolean responseErrorCode(WLResponse response) {
        final int status = response.getStatus();
        return (status != 200 && status != 206 && status != 404 && status != 401);
    }

    protected abstract void onSuccessResponse(WLResponse wlResponse);

    protected abstract void onSuccessError(WLFailResponse wlFailResponse);

}