package com.globant.cencosudsupport.network.request;

/**
 * Created by mariano.salvetti on 11/3/2017
 * Base callback to process errors
 */

public interface IBaseCallback {

    void onError(String error);
}
