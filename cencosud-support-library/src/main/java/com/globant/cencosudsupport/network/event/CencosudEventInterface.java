package com.globant.cencosudsupport.network.event;

/**
 * Created by edwin.peraza on 10/23/2017.
 */

public interface CencosudEventInterface {

    boolean isFaulted();

    void setIsFaulted(boolean value);

    String getMessage();

    void setMessage(String message);

}