package com.globant.cencosudsupport.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by raulstriglio on 6/9/17.
 */

public class ErrorResponse {

    @SerializedName("message")
    String mMessage;

    public String getMessage() {
        return mMessage;
    }

}
