package com.globant.cencosudsupport.network.request;

import com.globant.cencosudsupport.network.event.CencosudEventInterface;
import com.squareup.otto.Bus;
import com.worklight.wlclient.api.WLResourceRequest;
import com.worklight.wlclient.api.WLResponseListener;

import java.util.HashMap;

/**
 * Created by raulstriglio on 6/9/17
 * changed by Mariano Salvetti and Edwin Peraza
 */

public abstract class BaseRequest {

    /**
     * Base method to send request by IBM MF
     */
    public abstract void sendRequest(final Bus bus, HashMap<String, String> paramsMaps);

    /**
     * Version 2 method to send request by IBM MF
     *
     * @param bus                to send event
     * @param paramsMaps         key value to be send via body
     * @param eventInterface     event when throw exception
     * @param wlResponseListener listener to manage response
     */
    public void sendRequest(final Bus bus,
                            HashMap<String, String> paramsMaps,
                            CencosudEventInterface eventInterface,
                            WLResponseListener wlResponseListener) {
    }

    /**
     * Implements build pattern using ResourceRequestCencosud class
     *
     * @param resourceRequest to be used for IMB MF
     */
    public void sendRequest(WLResourceRequest resourceRequest) {
    }

}
