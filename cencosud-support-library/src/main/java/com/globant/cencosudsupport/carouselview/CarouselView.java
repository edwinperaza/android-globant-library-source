package com.globant.cencosudsupport.carouselview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews.RemoteView;

import com.globant.cencosudsupport.swiperefresh.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Mariano Salvetti
 *
 * ref R.styleable#CarouselView_pageTransformer
 */
@RemoteView
public class CarouselView extends FrameLayout {

    private static final int DEFAULT_GRAVITY = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;

    private static final int DEFAULT_SLIDE_INTERVAL = 3500;
    private static final int DEFAULT_SLIDE_VELOCITY = 400;


    private int mPageCount;
    private int mSlideInterval = DEFAULT_SLIDE_INTERVAL;
    private int mIndicatorGravity = DEFAULT_GRAVITY;
    private int mIndicatorMarginVertical;
    private int mIndicatorMarginHorizontal;
    private int mPageTransformInterval = DEFAULT_SLIDE_VELOCITY;

    private CarouselViewPager mContainerViewPager;
    private CirclePageIndicator mIndicator;
    private ViewListener mViewListener = null;
    private ImageListener mImageListener = null;

    private Timer mSwipeTimer;
    private SwipeTask mSwipeTask;

    private boolean mAutoPlay;
    private boolean mDisableAutoPlayOnUserInteraction;
    private boolean mAnimateOnBoundary = true;

    private int mPreviousState;

    private ViewPager.PageTransformer mPageTransformer;

    public CarouselView(Context context) {
        super(context);
    }

    public CarouselView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, 0, 0);
    }

    public CarouselView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CarouselView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        if (isInEditMode()) {
            return;
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.view_carousel, this, true);
            mContainerViewPager = (CarouselViewPager) view.findViewById(R.id.containerViewPager);
            mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);

            mContainerViewPager.addOnPageChangeListener(carouselOnPageChangeListener);


        }
        //Retrieve styles attributes
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CarouselView, defStyleAttr, 0);
        try {
            setmIndicatorMarginVertical(a.getInt(
                    R.styleable.CarouselView_indicatorMarginVertical, getResources().getDimensionPixelSize(R.dimen.default_indicator_margin_vertical)));
            setmIndicatorMarginHorizontal(a.getInt(
                    R.styleable.CarouselView_indicatorMarginHorizontal, getResources().getDimensionPixelSize(R.dimen.default_indicator_margin_horizontal)));
            setmPageTransformInterval(a.getInt(
                    R.styleable.CarouselView_mPageTransformInterval, DEFAULT_SLIDE_VELOCITY));
            setmSlideInterval(a.getInt(
                    R.styleable.CarouselView_mSlideInterval, DEFAULT_SLIDE_INTERVAL));
            setOrientation(a.getInt(
                    R.styleable.CarouselView_indicatorOrientation, LinearLayout.HORIZONTAL));
            setIndicatorGravity(a.getInt(
                    R.styleable.CarouselView_indicatorGravity,
                    Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL));
            setmAutoPlay(a.getBoolean(
                    R.styleable.CarouselView_mAutoPlay, true));
            setmDisableAutoPlayOnUserInteraction(a.getBoolean(
                    R.styleable.CarouselView_mDisableAutoPlayOnUserInteraction, false));
            setmAnimateOnBoundary(a.getBoolean(
                    R.styleable.CarouselView_mAnimateOnBoundary, true));


            setPageTransformer(a.getInt(
                    R.styleable.CarouselView_mPageTransformer,
                    CarouselViewPagerTransformer.DEFAULT));

            int fillColor = a.getColor(R.styleable.CarouselView_fillColor, 0);
            if (fillColor != 0) {
                setFillColor(fillColor);
            }
            int pageColor = a.getColor(R.styleable.CarouselView_pageColor, 0);
            if (pageColor != 0) {
                setPageColor(pageColor);
            }
            float radius = a.getDimensionPixelSize(R.styleable.CarouselView_radius, 0);
            if (radius != 0) {
                setRadius(radius);
            }
            setSnap(a.getBoolean(R.styleable.CarouselView_snap, getResources().
                    getBoolean(R.bool.default_circle_indicator_snap)));
            int strokeColor = a.getColor(R.styleable.CarouselView_strokeColor, 0);
            if (strokeColor != 0) {
                setStrokeColor(strokeColor);
            }
            float strokeWidth = a.getDimensionPixelSize(
                    R.styleable.CarouselView_strokeWidth, 0);
            if (strokeWidth != 0) {
                setStrokeWidth(strokeWidth);
            }

        } finally {
            a.recycle();
        }
    }

    public int getmSlideInterval() {
        return mSlideInterval;
    }

    /**
     * Set interval for one slide in milliseconds.
     *
     * @param mSlideInterval integer
     */
    public void setmSlideInterval(int mSlideInterval) {
        this.mSlideInterval = mSlideInterval;

        if (null != mContainerViewPager) {
            playCarousel();
        }
    }

    /**
     * Set interval for one slide in milliseconds.
     *
     * @param slideInterval integer
     */
    public void reSetSlideInterval(int slideInterval) {
        setmSlideInterval(slideInterval);

        if (null != mContainerViewPager) {
            playCarousel();
        }
    }

    /**
     * Sets speed at which page will slide from one to another in milliseconds
     *
     * @param mPageTransformInterval integer
     */
    public void setmPageTransformInterval(int mPageTransformInterval) {
        if (mPageTransformInterval > 0) {
            this.mPageTransformInterval = mPageTransformInterval;
        } else {
            this.mPageTransformInterval = DEFAULT_SLIDE_VELOCITY;
        }

        mContainerViewPager.setTransitionVelocity(mPageTransformInterval);
    }

    public ViewPager.PageTransformer getmPageTransformer() {
        return mPageTransformer;
    }

    /**
     * Sets page transition animation.
     *
     * @param mPageTransformer Choose from zoom, flow, depth, slide_over .
     */
    public void setmPageTransformer(ViewPager.PageTransformer mPageTransformer) {
        this.mPageTransformer = mPageTransformer;
        mContainerViewPager.setPageTransformer(true, mPageTransformer);
    }

    /**
     * Sets page transition animation.
     *
     * @param transformer Pass {@link CarouselViewPagerTransformer#FLOW}, {@link CarouselViewPagerTransformer#ZOOM}, {@link CarouselViewPagerTransformer#DEPTH} or {@link CarouselViewPagerTransformer#SLIDE_OVER}
     *
     */
    public void setPageTransformer(@CarouselViewPagerTransformer.Transformer int transformer) {
        setmPageTransformer(new CarouselViewPagerTransformer(transformer));

    }

    /**
     * Sets whether to animate transition from last position to first or not.
     *
     * @param mAnimateOnBoundary .
     */
    public void setmAnimateOnBoundary(boolean mAnimateOnBoundary) {
        this.mAnimateOnBoundary = mAnimateOnBoundary;
    }

    public boolean ismAutoPlay() {
        return mAutoPlay;
    }

    private void setmAutoPlay(boolean mAutoPlay) {
        this.mAutoPlay = mAutoPlay;
    }

    public boolean ismDisableAutoPlayOnUserInteraction() {
        return mDisableAutoPlayOnUserInteraction;
    }

    private void setmDisableAutoPlayOnUserInteraction(boolean mDisableAutoPlayOnUserInteraction) {
        this.mDisableAutoPlayOnUserInteraction = mDisableAutoPlayOnUserInteraction;
    }

    private void setData() {
        CarouselPagerAdapter carouselPagerAdapter = new CarouselPagerAdapter(getContext());
        mContainerViewPager.setAdapter(carouselPagerAdapter);
        mIndicator.setViewPager(mContainerViewPager);
        mIndicator.requestLayout();
        mIndicator.invalidate();
        mContainerViewPager.setOffscreenPageLimit(getPageCount());
        playCarousel();
    }

    private void stopScrollTimer() {

        if (null != mSwipeTimer) {
            mSwipeTimer.cancel();
        }

        if (null != mSwipeTask) {
            mSwipeTask.cancel();
        }
    }


    private void resetScrollTimer() {

        stopScrollTimer();

        mSwipeTask = new SwipeTask();
        mSwipeTimer = new Timer();

    }

    /**
     * Starts auto scrolling if
     */
    public void playCarousel() {

        resetScrollTimer();

        if (mAutoPlay && mSlideInterval > 0 && mContainerViewPager.getAdapter() != null && mContainerViewPager.getAdapter().getCount() > 1) {

            mSwipeTimer.schedule(mSwipeTask, mSlideInterval, mSlideInterval);
        }
    }

    /**
     * Pause auto scrolling unless user interacts provided mAutoPlay is enabled.
     */
    public void pauseCarousel() {

        resetScrollTimer();
    }

    /**
     * Stops auto scrolling.
     */
    public void stopCarousel() {

        this.mAutoPlay = false;
    }


    private class CarouselPagerAdapter extends PagerAdapter {
        private Context mContext;

        public CarouselPagerAdapter(Context context) {
            mContext = context;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {

            Object objectToReturn;

            //Either let user set image to ImageView
            if (mImageListener != null) {

                ImageView imageView = new ImageView(mContext);
                imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));  //setting image position
                imageView.setPadding(10,10,10,40);
                objectToReturn = imageView;
                mImageListener.setImageForPosition(position, imageView);

                collection.addView(imageView);

                //Or let user add his own ViewGroup
            } else if (mViewListener != null) {

                View view = mViewListener.setViewForPosition(position);

                if (null != view) {
                    objectToReturn = view;
                    collection.addView(view);
                } else {
                    throw new RuntimeException("View can not be null for position " + position);
                }

            } else {
                throw new RuntimeException("View must set " + ImageListener.class.getSimpleName() + " or " + ViewListener.class.getSimpleName() + ".");
            }

            return objectToReturn;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getCount() {
            return getPageCount();
        }
    }

    ViewPager.OnPageChangeListener carouselOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            //Programmatic scroll

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

            //User initiated scroll

            if (mPreviousState == ViewPager.SCROLL_STATE_DRAGGING
                    && state == ViewPager.SCROLL_STATE_SETTLING) {

                if (mDisableAutoPlayOnUserInteraction) {
                    pauseCarousel();
                } else {
                    playCarousel();
                }

            } else if (mPreviousState == ViewPager.SCROLL_STATE_SETTLING
                    && state == ViewPager.SCROLL_STATE_IDLE) {
            }

            mPreviousState = state;

        }
    };

    private class SwipeTask extends TimerTask {
        public void run() {
            mContainerViewPager.post(new Runnable() {
                public void run() {

                    int nextPage = (mContainerViewPager.getCurrentItem() + 1) % getPageCount();

                    mContainerViewPager.setCurrentItem(nextPage, 0 != nextPage || mAnimateOnBoundary);
                }
            });
        }
    }

    public void setImageListener(ImageListener mImageListener) {
        this.mImageListener = mImageListener;
    }

    public void setViewListener(ViewListener mViewListener) {
        this.mViewListener = mViewListener;
    }

    public int getPageCount() {
        return mPageCount;
    }

    public void setPageCount(int mPageCount) {
        this.mPageCount = mPageCount;

        setData();
    }

    public void addOnPageChangeListener(ViewPager.OnPageChangeListener listener) {
        mContainerViewPager.addOnPageChangeListener(listener);
    }

    public void setCurrentItem(int item) {
        mContainerViewPager.setCurrentItem(item);
    }

    public int getCurrentItem() {
        return mContainerViewPager.getCurrentItem();
    }

    public int getmIndicatorMarginVertical() {
        return mIndicatorMarginVertical;
    }

    public void setmIndicatorMarginVertical(int _indicatorMarginVertical) {
        mIndicatorMarginVertical = _indicatorMarginVertical;
    }

    public int getmIndicatorMarginHorizontal() {
        return mIndicatorMarginHorizontal;
    }

    public void setmIndicatorMarginHorizontal(int _indicatorMarginHorizontal) {
        mIndicatorMarginHorizontal = _indicatorMarginHorizontal;
    }

    public int getIndicatorGravity() {
        return mIndicatorGravity;
    }

    public void setIndicatorGravity(int gravity) {
        mIndicatorGravity = gravity;
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = mIndicatorGravity;
        params.setMargins(mIndicatorMarginHorizontal, mIndicatorMarginVertical, mIndicatorMarginHorizontal, mIndicatorMarginVertical);
        mIndicator.setLayoutParams(params);
    }

    public int getOrientation() {
        return mIndicator.getOrientation();
    }

    public int getFillColor() {
        return mIndicator.getFillColor();
    }

    public int getStrokeColor() {
        return mIndicator.getStrokeColor();
    }

    public void setSnap(boolean snap) {
        mIndicator.setSnap(snap);
    }

    public void setRadius(float radius) {
        mIndicator.setRadius(radius);
    }

    public float getStrokeWidth() {
        return mIndicator.getStrokeWidth();
    }

    @Override
    public void setBackground(Drawable background) {
        super.setBackground(background);
    }

    public Drawable getIndicatorBackground() {
        return mIndicator.getBackground();
    }

    public void setFillColor(int fillColor) {
        mIndicator.setFillColor(fillColor);
    }

    public int getPageColor() {
        return mIndicator.getPageColor();
    }

    public void setOrientation(int orientation) {
        mIndicator.setOrientation(orientation);
    }

    public boolean isSnap() {
        return mIndicator.isSnap();
    }

    public void setStrokeColor(int strokeColor) {
        mIndicator.setStrokeColor(strokeColor);
    }

    public float getRadius() {
        return mIndicator.getRadius();
    }

    public void setPageColor(int pageColor) {
        mIndicator.setPageColor(pageColor);
    }

    public void setStrokeWidth(float strokeWidth) {
        mIndicator.setStrokeWidth(strokeWidth);
        int padding = (int) strokeWidth;
        mIndicator.setPadding(padding, padding, padding, padding);
    }
}