package com.globant.cencosudsupport.carouselview;

import android.view.View;

/**
 * Created by Mariano Salvetti on 3/17/16.
 * To set custom View in CarouselView
 */
public interface ViewListener {

    View setViewForPosition(int position);
}
