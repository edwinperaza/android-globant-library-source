package com.globant.cencosudsupport.carouselview;

import android.widget.ImageView;

/**
 * Created by Mariano Salvetti on 3/15/16.
 * To set simple ImageView drawable in CarouselView
 */
public interface ImageListener {

    void setImageForPosition(int position, ImageView imageView);

}
