package com.globant.cencosudsupport.utils;

import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;

import java.lang.ref.WeakReference;

/**
 * Created by brandon on 30/08/17.
 */

public class PhoneNumberFormatterTextWatcher implements TextWatcher {
    private static final String WHITE_SPACE = " ";
    private boolean mFormatting;
    private int mLastStartLocation;
    private String mLastBeforeText;
    private WeakReference<TextInputEditText> mWeakTextInputEditText;

    public PhoneNumberFormatterTextWatcher(WeakReference<TextInputEditText> weakEditText) {
        this.mWeakTextInputEditText = weakEditText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        mLastStartLocation = start;
        mLastBeforeText = s.toString();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before,
                              int count) {
        // TODO: Do nothing
    }

    @Override
    public void afterTextChanged(Editable s) {
        // Make sure to ignore calls to afterTextChanged caused by the work
        // done below
        if (!mFormatting) {
            mFormatting = true;
            int curPos = mLastStartLocation;
            String beforeValue = mLastBeforeText;
            String currentValue = s.toString();
            String formattedValue = formatNumber(s);
            if (currentValue.length() > beforeValue.length()) {
                int setCusorPos = formattedValue.length() - (beforeValue.length() - curPos);
                mWeakTextInputEditText.get().setSelection(setCusorPos < 0 ? 0 : setCusorPos);
            } else {
                int setCusorPos = formattedValue.length() - (currentValue.length() - curPos);
                mWeakTextInputEditText.get().setSelection(setCusorPos < 0 ? 0 : setCusorPos);
            }
            mFormatting = false;
        }
    }

    private String formatNumber(Editable text) {
        StringBuilder formattedString = new StringBuilder();
        // Remove everything except digits and + symbol
        int p = 0;
        while (p < text.length()) {
            char ch = text.charAt(p);
            if (Character.isDigit(ch) || Character.toString(ch).equals("+")) {
                p++;
            } else {
                text.delete(p, p + 1);
            }
        }

        String allDigitString = text.toString();

        int totalDigitCount = allDigitString.length();

        if (totalDigitCount == 9) {
            formattedString.append(allDigitString.substring(0, 1));
            formattedString.append(WHITE_SPACE);
            formattedString.append(allDigitString.substring(1, totalDigitCount));
        } else if (totalDigitCount == 12) {
            formattedString.append(allDigitString.substring(0, 4));
            formattedString.append(WHITE_SPACE);
            formattedString.append(allDigitString.substring(4, totalDigitCount));
        } else {
            formattedString.append(allDigitString);
        }

        text.clear();
        text.append(formattedString.toString());
        return formattedString.toString();
    }

}
