package com.globant.cencosudsupport.utils.JsonDecoder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by raul.striglio on 20/07/17.
 */

public class GsonDecoder<T> {

    private GsonBuilder mGsonBuilder;
    private Gson mGson;

    public GsonDecoder() {
        mGsonBuilder = new GsonBuilder();
        mGsonBuilder.registerTypeAdapterFactory(new OptionalTypeAdapterFactory());
        mGson = mGsonBuilder.create();
    }

    public Gson getGson() {
        return mGson;
    }

}
