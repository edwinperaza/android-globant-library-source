package com.globant.cencosudsupport.utils;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by diego.stroppiana on 28/6/2017.
 */

public class Validator {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(target);
            return matcher.find();
        }
    }

    public static boolean validateRut(CharSequence target) {
        if (target.length() == 0) //null RUT
            return false;
        else
            return !(target.length() < 8 || target.length() > 9) && TextUtils.isDigitsOnly(target.subSequence(0, target.length() - 1));

    }

}
