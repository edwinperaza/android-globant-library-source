package com.globant.cencosudsupport.utils.JsonDecoder;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by raul.striglio on 20/07/17.
 */

public class OptionalTypeAdapterFactory implements TypeAdapterFactory {
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        if (Optional.class.isAssignableFrom(type.getRawType())) {
            return new OptionalTypeAdapter<T>(gson, type);
        }
        return null;
    }
}

class OptionalTypeAdapter<T> extends TypeAdapter<T> {
    private final Gson mGson;
    private final Type mWrappedType;

    public OptionalTypeAdapter(Gson mGson, TypeToken<T> type) {
        this.mGson = mGson;
        this.mWrappedType = ((ParameterizedType) type.getType()).getActualTypeArguments()[0];
    }

    @Override
    public void write(JsonWriter writer, T value) throws IOException {
        final Optional<?> optional = (Optional<?>) value;
        if (optional != null) {
            mGson.toJson(optional.orNull(), mWrappedType, writer);
        } else {
            boolean serializeNulls = writer.getSerializeNulls();
            writer.setSerializeNulls(false);
            writer.nullValue();
            writer.setSerializeNulls(serializeNulls);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T read(JsonReader reader) throws IOException {
        final Object value = mGson.fromJson(reader, mWrappedType);
        return (T) Optional.fromNullable(value);
    }
}