package com.globant.cencosudsupport.utils;

import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import java.lang.ref.WeakReference;

/**
 * Created by jb.vargas on 28/09/17.
 */

public class RutTextWatcher implements TextWatcher, View.OnKeyListener {
    private static final String TAG = "RutTextWatcher";
    private static final String RUT_PATTERN = "^[0-9-.kK]*$";
    private static final String WHITE_SPACE = " ";
    private boolean mFormatting;
    private WeakReference<TextInputEditText> mWeakTextInputEditText;
    private boolean keyWasBack;

    private int position;

    public RutTextWatcher(WeakReference<TextInputEditText> weakEditText) {
        this.mWeakTextInputEditText = weakEditText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        position = mWeakTextInputEditText.get().getSelectionStart();
        Log.d(TAG, "Prev position: " + position);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before,
                              int count) {
        // TODO: Do nothing
    }

    @Override
    public void afterTextChanged(Editable s) {
        // Make sure to ignore calls to afterTextChanged caused by the work
        // done below
        if (!mFormatting) {
            mFormatting = true;
            formatRut(s);
            mFormatting = false;
        }
    }

    private void formatRut(Editable text) {
        if (!text.toString().matches(RUT_PATTERN)) {
            return;
        }

        if (keyWasBack) {
            keyWasBack = false;
            return;
        }

        Log.d(TAG, "FORMATTING RUT!");

        StringBuilder formattedString = new StringBuilder();
        String allInputString = text.toString();
        String onlyNumbersOrChars = allInputString
                .replace(".", "")
                .replace("-", "");

        //int totalDigitCount = allInputString.length();
        if (Validator.validateRut(onlyNumbersOrChars)) {
            formattedString.append(StringUtils.getMaskedRut(onlyNumbersOrChars));
        } else {
            formattedString.append(allInputString);
        }

        text.clear();
        text.append(formattedString.toString());
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            keyWasBack = true;
        } else {
            keyWasBack = false;
        }
        return false;
    }
}
