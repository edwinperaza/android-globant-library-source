package com.globant.cencosudsupport.utils;

import android.text.TextUtils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by brandon on 5/07/17.
 */

public class StringUtils {

    private static final String CHAR_PERIOD = ".";
    private static final String CHAR_DASH = "-";
    private static final String WHITE_SPACE = " ";

    public static String formatMoney(double amount) {
        int truncatedAmount = (int) Math.floor(amount);
        Locale locale = new Locale("es", "CL"); //Chile
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
        return nf.format(truncatedAmount);
    }

    public static String formatMoney(int amount) {
        Locale locale = new Locale("es", "CL"); //Chile
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
        return nf.format(amount);
    }

    /**
     * Given a string that matches a valid chilean RUT number
     * returns the number with the correct format
     *
     * @param rut
     * @return string
     */
    public static String getMaskedRut(String rut) {
        String result;
        if (rut.length() == 0) { //null RUT
            return "--";
        } else if (rut.length() < 8 || rut.length() > 9 ||
                !TextUtils.isDigitsOnly(rut.substring(0, rut.length() - 1))) { //invalid RUT, can't be formatted.
            return rut;
        }
        StringBuilder formattedRutBuilder = new StringBuilder();
        if (rut.length() == 8) {
            formattedRutBuilder.append(rut.substring(0, 1));
            formattedRutBuilder.append(CHAR_PERIOD);
            formattedRutBuilder.append(rut.substring(1, 4));
            formattedRutBuilder.append(CHAR_PERIOD);
            formattedRutBuilder.append(rut.substring(4, 7));
            formattedRutBuilder.append(CHAR_DASH);
            formattedRutBuilder.append(rut.substring(7, 8));
        } else {
            formattedRutBuilder.append(rut.substring(0, 2));
            formattedRutBuilder.append(CHAR_PERIOD);
            formattedRutBuilder.append(rut.substring(2, 5));
            formattedRutBuilder.append(CHAR_PERIOD);
            formattedRutBuilder.append(rut.substring(5, 8));
            formattedRutBuilder.append(CHAR_DASH);
            formattedRutBuilder.append(rut.substring(8, 9));
        }
        result = formattedRutBuilder.toString();
        return result;
    }
}
