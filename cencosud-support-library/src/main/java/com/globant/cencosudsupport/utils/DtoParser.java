package com.globant.cencosudsupport.utils;

import com.google.common.base.Optional;

/**
 * Created by edwin.peraza on 8/8/2017.
 */

public class DtoParser<T> {

    public T parseValue(Optional<T> param, T defaultValue) {
        T returnValue;
        if (param == null || (!param.isPresent())) {
            returnValue = defaultValue;
        } else {
            returnValue = param.get();
        }
        return returnValue;
    }
}