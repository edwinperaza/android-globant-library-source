package com.globant.cencosudsupport.utils;

import android.view.View;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;

/**
 * Created by edwin.peraza on 7/13/2017.
 */

public class CartAnimator {

    public void updateCartNumber(final int newNumber, final TextView mMenuCartText) {
        int currentNumber = 0;
        if (mMenuCartText != null) {

            currentNumber = Integer.parseInt(mMenuCartText.getText().toString());
            if (!(newNumber == currentNumber)) {
                if (newNumber > 0) {
                    if (currentNumber > newNumber) {


                        YoYo.with(Techniques.RotateOutDownLeft).duration(200).withListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                mMenuCartText.setVisibility(View.GONE);
                                mMenuCartText.setText(String.valueOf(newNumber));
                                YoYo.with(Techniques.RollIn).duration(200).withListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        super.onAnimationStart(animation);
                                        mMenuCartText.setVisibility(View.VISIBLE);
                                    }
                                }).playOn(mMenuCartText);
                            }
                        }).playOn(mMenuCartText);
                    } else {


                        if (mMenuCartText.getVisibility() == View.VISIBLE) {
                            YoYo.with(Techniques.RotateOutDownLeft).duration(200).withListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    mMenuCartText.setVisibility(View.GONE);
                                    mMenuCartText.setText(String.valueOf(newNumber));
                                    YoYo.with(Techniques.RollIn).duration(200).withListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {
                                            super.onAnimationStart(animation);
                                            mMenuCartText.setVisibility(View.VISIBLE);
                                        }
                                    }).playOn(mMenuCartText);
                                }
                            }).playOn(mMenuCartText);
                        } else {
                            mMenuCartText.setText(String.valueOf(newNumber));
                            YoYo.with(Techniques.RollIn).duration(200).withListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    super.onAnimationStart(animation);
                                    mMenuCartText.setVisibility(View.VISIBLE);
                                }
                            }).playOn(mMenuCartText);
                        }
                    }
                } else {
                    if (currentNumber == 0) {
                        mMenuCartText.setVisibility(View.GONE);
                    } else {

                        YoYo.with(Techniques.RotateOutDownLeft).duration(200).withListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                mMenuCartText.setVisibility(View.GONE);
                                mMenuCartText.setText(String.valueOf(newNumber));
                            }
                        }).playOn(mMenuCartText);
                    }

                }
            }
        }
    }

    public static void vibratePhone() {
        //Vibrate
//        Vibrator v = (Vibrator) BaseInkaApplication.getInstance().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
//        v.vibrate(350);
    }
}
