package com.globant.cencosudsupport.adapterDelegate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by raul.striglio on 04/08/16.
 */
public class AdapterDelegatesManager<T> {

    private Context mContext;
    private SparseArrayCompat<AdapterDelegate<T>> mListDelegates;

    public AdapterDelegatesManager(Context context) {
        this.mContext = context;
        mListDelegates = new SparseArrayCompat();
    }

    public AdapterDelegatesManager<T> addDelegate(@NonNull AdapterDelegate<T> delegate, @AdapterType.AdapterTypeEnumeration int viewType) {
        mListDelegates.put(viewType, delegate);
        return this;
    }

    public void removeDelegate(@NonNull AdapterDelegate<T> delegate) {
        int index = mListDelegates.indexOfValue(delegate);
        if (index >= 0) {
            mListDelegates.removeAt(index);
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AdapterDelegate<T> delegate = mListDelegates.get(viewType);
        return delegate.onCreateViewHolder(parent, viewType);
    }

    public void onBindViewHolder(@NonNull T items, int position, @NonNull RecyclerView.ViewHolder viewHolder) {
        AdapterDelegate<T> delegate = mListDelegates.get(viewHolder.getItemViewType());
        delegate.onBindViewHolder(items, position, viewHolder);
    }

}
