package com.globant.cencosudsupport.adapterDelegate;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by raul.striglio on 04/11/16.
 */

public final class AdapterType {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ADAPTER_PRODUCT, ADAPTER_PRODUCT_WITH_DISCOUNT, ADAPTER_EMPTY, ADAPTER_PRODUCT_RECOMMENDATIONS})
    @interface AdapterTypeEnumeration {
    }

    public static final int ADAPTER_PRODUCT = 1;
    public static final int ADAPTER_PRODUCT_WITH_DISCOUNT = 2;
    public static final int ADAPTER_EMPTY = 3;
    public static final int ADAPTER_PRODUCT_RECOMMENDATIONS = 4;

}
