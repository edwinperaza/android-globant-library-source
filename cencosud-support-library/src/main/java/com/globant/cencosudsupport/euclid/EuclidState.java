package com.globant.cencosudsupport.euclid;

/**
 * Created by Jotta Reyes Blanco on 11/4/2016.
 */

public enum EuclidState {
    Closed, Opening, Opened, Closing
}
