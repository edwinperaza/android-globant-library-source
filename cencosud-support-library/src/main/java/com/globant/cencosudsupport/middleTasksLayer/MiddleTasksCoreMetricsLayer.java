package com.globant.cencosudsupport.middleTasksLayer;

import bolts.Continuation;
import bolts.Task;

/**
 * Created by Mariano Salvetti
 *
 */

/**
 * Class that MUST be used in the Presenter class, to perform GET operations to the CorerMetric.
 * @param <T> T is the data type that we expect to receive from the GET operation performed
 *           through this class
 */

public class MiddleTasksCoreMetricsLayer<T> implements IMiddleTaskGet {

    private static final String EMPTY_STRING = "";

    private MiddleTasksCallbacksLayer.CoreMetricsTaksCallback mGetTasksCalback; //this object is received from Presenter Class

    public MiddleTasksCoreMetricsLayer(MiddleTasksCallbacksLayer.CoreMetricsTaksCallback getTasksCalback) {
        mGetTasksCalback = getTasksCalback;
    }

    @Override
    public void callGetTask(Object... params) {
        final Task<T> mTask = mGetTasksCalback.fireEventRegistration(params);
        mTask.onSuccess(new Continuation<T, Void>() {
                    public Void then(Task<T> task) throws Exception {
                        if (mTask.isFaulted()) {
                            mGetTasksCalback.resultError(mTask.getError().getMessage());
                        } else {
                            if (mTask.isCompleted()) {
                                if (mTask.getResult() != null) {
                                    mGetTasksCalback.resultSuccess(mTask.getResult());
                                } else {
                                    mGetTasksCalback.resultError(EMPTY_STRING);
                                }
                            }
                        }
                        return null;
                    }
                }
                , Task.BACKGROUND_EXECUTOR);
    }
}
