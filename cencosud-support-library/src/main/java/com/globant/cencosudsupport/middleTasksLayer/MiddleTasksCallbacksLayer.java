package com.globant.cencosudsupport.middleTasksLayer;

import org.json.JSONObject;

import bolts.Task;

/**
 * Created by raul.striglio on 02/02/17.
 */


/**
 *
 * Class that defines three specifics callbacks to perform GET, PUT or LOGIN request to the server
 * Presenter class MUST implement one of this interfaces
 */

public class MiddleTasksCallbacksLayer {

    public interface PutTaksCallback<T> {
        Task<T> updateAsync(T body);

        void resultSuccess();

        void onError();
    }

    public interface GetTasksCalback<T> {

        //Task<T> downloadAsync(HashMap<String, String> params);

        Task<T> downloadAsync(Object... params);

        void resultSuccess(T result);

        void onError(String error);
    }

    public interface LoginTaksCallback<T> {
        Task<T> loginSync(T securityCheck, JSONObject credentials);

        void resultSuccess();

        void onLoginError();
    }

    public interface CoreMetricsTaksCallback<T> {
        Task<T> fireEventRegistration(Object... params);

        Task<T> firePageViewEvent(Object... params);

        Task<T> fireProductViewEvent(Object... params);

        Task<T> fireShopAction5EventEvent(Object... params);

        Task<T> fireConversionEvent(Object... params);

        void resultSuccess(T result);

        void resultError(String error);
    }
}
