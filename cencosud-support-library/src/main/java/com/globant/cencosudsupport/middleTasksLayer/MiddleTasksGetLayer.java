package com.globant.cencosudsupport.middleTasksLayer;

import bolts.Continuation;
import bolts.Task;

/**
 * Created by raul.striglio on 02/02/17.
 */

/**
 * Class that MUST be used in the Presenter class, to perform GET operations to the server.
 * @param <T> T is the data type that we expect to receive from the GET operation performed through this class
 */

public class MiddleTasksGetLayer<T> implements IMiddleTaskGet {

    private final String EMPTY_STRING = "";

    private MiddleTasksCallbacksLayer.GetTasksCalback mGetTasksCalback; //this object is received from Presenter Class

    public MiddleTasksGetLayer(MiddleTasksCallbacksLayer.GetTasksCalback getTasksCalback) {
        mGetTasksCalback = getTasksCalback;
    }

    @Override
    public void callGetTask(Object... params) {
        final Task<T> mTask = mGetTasksCalback.downloadAsync(params);
        onTaskContinuation(mTask);
    }

    private void onTaskContinuation(final Task<T> mTask) {
        mTask.onSuccess(new Continuation<T, Void>() {
                            public Void then(Task<T> task) throws Exception {
                                if (mTask.isFaulted()) {
                                    mGetTasksCalback.onError(mTask.getError().getMessage());
                                } else {
                                    if (mTask.isCompleted()) {
                                        if (mTask.getResult() != null) {
                                            mGetTasksCalback.resultSuccess(mTask.getResult());
                                        } else {
                                            mGetTasksCalback.onError(EMPTY_STRING);
                                        }
                                    }
                                }
                                return null;
                            }
                        }

                , Task.UI_THREAD_EXECUTOR);
    }
}
