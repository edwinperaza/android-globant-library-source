package com.globant.cencosudsupport.middleTasksLayer;

import org.json.JSONObject;

import bolts.Continuation;
import bolts.Task;

/**
 * Created by Mariano Salvetti
 *
 */

public class MiddleTasksLoginLayer<T> {

    private MiddleTasksCallbacksLayer.LoginTaksCallback<T> mLoginTaksCallback;

    public MiddleTasksLoginLayer(MiddleTasksCallbacksLayer.LoginTaksCallback<T> loginTaksCallback) {
        mLoginTaksCallback = loginTaksCallback;
    }

    public void callLoginTask(T securityCheck, JSONObject credentials) {

        final Task<T> mTask = mLoginTaksCallback.loginSync(securityCheck, credentials);
        mTask.onSuccess(new Continuation<T, Void>() {
                            public Void then(Task<T> task) throws Exception {
                                if (mTask.isFaulted()) {
                                    mLoginTaksCallback.onLoginError();
                                } else {
                                    if (mTask.isCompleted()) {
                                        if (mTask.getResult() != null) {
                                            mLoginTaksCallback.resultSuccess();
                                        } else {
                                            mLoginTaksCallback.onLoginError();
                                        }
                                    }
                                }
                                return null;
                            }
                        }

                , Task.UI_THREAD_EXECUTOR);
    }


}
