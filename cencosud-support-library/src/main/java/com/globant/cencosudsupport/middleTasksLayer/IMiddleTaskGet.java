package com.globant.cencosudsupport.middleTasksLayer;

/**
 * Created by raulstriglio on 2/3/17.
 */

/**
 * Interface that should be implemented to define callGetTask
 * to perform GET request to the server
 */

public interface IMiddleTaskGet {

    //void callGetTask(HashMap<String, String> requestParams);

    void callGetTask(Object... param);
}
