package com.globant.cencosudsupport.middleTasksLayer;

import bolts.Continuation;
import bolts.Task;

/**
 * Created by raulstriglio on 2/3/17.
 */

public class MiddleTasksPutLayer<T> {

    private MiddleTasksCallbacksLayer.PutTaksCallback<T> mPutTaksCallback;

    public MiddleTasksPutLayer(MiddleTasksCallbacksLayer.PutTaksCallback<T> putTaksCallback){
        mPutTaksCallback = putTaksCallback;
    }

    public void callPutTask(T body) {

        final Task<T> mTask = mPutTaksCallback.updateAsync(body);
        mTask.onSuccess(new Continuation<T, Void>() {
                            public Void then(Task<T> task) throws Exception {
                                if (mTask.isFaulted()) {
                                    mPutTaksCallback.onError();
                                } else {
                                    if (mTask.isCompleted()) {
                                        if (mTask.getResult() != null) {
                                            mPutTaksCallback.resultSuccess();
                                        } else {
                                            mPutTaksCallback.onError();
                                        }
                                    }
                                }
                                return null;
                            }
                        }

                , Task.UI_THREAD_EXECUTOR);
    }


}
