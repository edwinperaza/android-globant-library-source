package com.globant.cencosudsupport.swipeablerecyclerview;

import android.support.v7.widget.RecyclerView;

import com.github.brnunes.swipeablerecyclerview.SwipeableRecyclerViewTouchListener;

/**
 * Created by raulstriglio on 1/11/17.
 */

public class SwipeableRecyclerView {

    public SwipeableRecyclerViewTouchListener createSwipeTocuhListener(RecyclerView recyclerView, final ISwipeCallback iSwipeCallback ){

        SwipeableRecyclerViewTouchListener swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(recyclerView,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {
                            @Override
                            public boolean canSwipeLeft(int position) {
                                return true;
                            }

                            @Override
                            public boolean canSwipeRight(int position) {
                                return true;
                            }

                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                iSwipeCallback.swipeLeftAction();
                            }

                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                iSwipeCallback.swipeRightAction();
                            }
                        });

        return swipeTouchListener;

    }

    public interface ISwipeCallback {
        void swipeLeftAction();
        void swipeRightAction();
    }

}
