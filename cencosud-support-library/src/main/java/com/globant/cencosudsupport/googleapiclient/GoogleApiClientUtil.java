package com.globant.cencosudsupport.googleapiclient;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by raul.striglio on 12/12/16.
 */

public class GoogleApiClientUtil implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int UPDATE_INTERVAL = 20 * 1000; // 10 seconds, in milliseconds
    private static final int FASTEST_INTERVAL = 1 * 1000; // 1 seconds, in milliseconds
    private GoogleApiClient mGoogleApiClient;
    private static GoogleApiClientUtil instance;
    private Location mLocation;
    private LocationRequest mLocationRequest;
    private Context mContext;

    private static final double DEFAULT_LATITUDE = -12.119261;
    private static final double DEFAULT_LONGITUDE = -77.025800;
    private static final float ZOOM_NO_LOC = 14f;
    private static final float ZOOM_LOC = 17f;

    private float zoom = ZOOM_LOC;

    private RegisterFenceInterface registerFenceInterface;

    private GoogleApiClientUtil(Context context) {

        mContext = context;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Awareness.API)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                                          .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                          .setInterval(UPDATE_INTERVAL)
                                          .setFastestInterval(FASTEST_INTERVAL); // 1 second, in milliseconds
    }

    public static GoogleApiClientUtil getInstance(Context context) {
        if (instance == null) {
            instance = new GoogleApiClientUtil(context);
        }
        return instance;
    }

    public void connect() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        } else {
            startLocationUpdates();
            registerFenceInterface.registerFence();
        }
    }

    public void disconnect() {
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        mGoogleApiClient.disconnect();
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            setCurrentLocation(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        registerFenceInterface.registerFence();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do nothing for now
        Toast.makeText(mContext, "Suspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Do nothing for now
        Toast.makeText(mContext, "Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            mLocation = location;
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            setCurrentLocation(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
        }else{
            zoom = ZOOM_LOC;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    public Location getCurrentLocation() {

        if(mLocation == null || !isLocationEnabled()){
            setCurrentLocation(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
        }
        return mLocation;
    }

    private void setCurrentLocation(double lat, double lng) {
        mLocation = new Location("default");
        mLocation.setLatitude(lat);
        mLocation.setLongitude(lng);
        zoom = ZOOM_NO_LOC;
    }

    public LatLng getMyLatLng() {
        return new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
    }

    public LatLng getLatLng(double latitud, double longitud) {
        return new LatLng(latitud, longitud);
    }

    public String getDistance(LatLng to_latlong) {
        Location l1 = new Location("One");
        l1.setLatitude(getMyLatLng().latitude);
        l1.setLongitude(getMyLatLng().longitude);

        Location l2 = new Location("Two");
        l2.setLatitude(to_latlong.latitude);
        l2.setLongitude(to_latlong.longitude);

        float distance = l1.distanceTo(l2);
        String dist = distance + " M";

        if (distance > 1000.0f) {
            distance = distance / 1000.0f;
            dist = distance + " KM";
        }
        return dist;
    }

    public boolean isLocationEnabled(){
        LocationManager lm = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        return gps_enabled || network_enabled;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public void setRegisterFenceInterface(RegisterFenceInterface registerFenceInterface) {
        this.registerFenceInterface = registerFenceInterface;
    }

    public interface RegisterFenceInterface {
        void registerFence();
    }
}
